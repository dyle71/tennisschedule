#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ------------------------------------------------------------
# tennisschedule/tennisschedule.py
#
# tennisschedule main working file
#
# (C) Copyright 2023, see the 'LICENSE' file in the project root.
# Oliver Maurhart, headcode.space, https://headcode.space
# ------------------------------------------------------------

"""This.is.tennisschedule!"""

import json
import random

import click
import tabulate

from . import __about__ as about


def convert_to_player_names(players: list, schedule: list) -> list:
    """Convert the indices in the schedule to player names."""
    plan = []
    for team1, team2 in schedule:
        player_1_a = players[team1[0]]['name']
        player_1_b = players[team1[1]]['name']
        player_2_a = players[team2[0]]['name']
        player_2_b = players[team2[1]]['name']
        plan.append(((player_1_a, player_1_b), (player_2_a, player_2_b)))
    return plan


def create_doubles(players: int) -> dict:
    """Create a dict of doubles.

    The keys into the dict are the tuples (player1, player2)
    whereas index of player1 is always less than the index
    of player2.

    :param players:     The number of players
    :return:            Dictionary of doubles.
    """
    doubles = {}
    for i in range(players):
        for j in range(i + 1, players):
            doubles[(i, j)] = 0

    return doubles


def get_min_games(players: list) -> int:
    """Return the minimum number of games played of all players."""
    return min([player['games'] for player in players])


def get_players_of_game(players: list, games: int) -> list:
    """Return the indices of players with the given number of played games."""
    return [ind for ind, player in enumerate(players) if player['games'] == games]


def load_players(filename: str) -> dict:
    """Load the players from a JSON file.

    :param filename:        The JSON file to load.
    :return:                Loaded JSON as dict.
    """
    with open(filename) as fp:
        return json.load(fp)


@click.command(context_settings={'help_option_names': ['-h', '--help']})
@click.version_option(version=about.__version__, package_name='tennisschedule', prog_name='tennisschedule')
@click.option('--player-file', type=click.Path(exists=True, file_okay=True, dir_okay=False, readable=True),
              help='Path the players JSON file.')
@click.option('--rounds', '-r', type=click.IntRange(min=1), show_default=True, default=10,
              help='Number of rounds')
@click.option('--doubles', '-d', is_flag=True, help='Also shuffle doubles')
@click.option('--tablefmt', type=str, show_default=True, default='simple',
              help='Output format as in https://pypi.org/project/tabulate/')
def main(player_file: str, rounds: int, doubles: bool, tablefmt: str) -> None:
    """tennisschedule - create rounds for a tennis tournament."""

    all_players = load_players(player_file)['players']
    if len(all_players) < 4:
        raise RuntimeError('I need at least 4 players for a tennis tournament.')

    for i in range(len(all_players)):
        all_players[i]['games'] = 0

    if doubles:
        run_doubles(players=all_players, rounds=rounds, tablefmt=tablefmt)
    else:
        run_normal(players=all_players, rounds=rounds, tablefmt=tablefmt)


def run_doubles(players: list, rounds: int, tablefmt: str) -> None:
    """Schedule rounds and also shuffle the doubles."""

    doubles = create_doubles(len(players))

    schedule = []

    for i in range(rounds):

        min_games = get_min_games(players)
        players_of_round = []

        while len(players_of_round) < 4:
            candidates = get_players_of_game(players=players, games=min_games)
            random.shuffle(candidates)
            needed_players = 4 - len(players_of_round)
            players_of_round += candidates[:needed_players]
            min_games += 1

        for j in range(len(players_of_round)):
            players[players_of_round[j]]['games'] += 1

        double_candidates = permutate_doubles(players_of_round)
        team1, team2 = pick_doubles(doubles, double_candidates)
        doubles[team1] += 1
        doubles[team2] += 1

        schedule.append((team1, team2))

    plan = convert_to_player_names(players, schedule)
    print(tabulate.tabulate(plan, headers=['Team1', 'Team2'], showindex="always", tablefmt=tablefmt))
    print(tabulate.tabulate(players, headers={'Player': 'Games'}, showindex="never", tablefmt=tablefmt))


def run_normal(players: list, rounds: int, tablefmt: str) -> None:
    """Schedule rounds."""

    schedule = []

    for i in range(rounds):

        min_games = get_min_games(players)
        players_of_round = []

        while len(players_of_round) < 4:
            candidates = get_players_of_game(players=players, games=min_games)
            random.shuffle(candidates)
            needed_players = 4 - len(players_of_round)
            players_of_round += candidates[:needed_players]
            min_games += 1

        for j in range(len(players_of_round)):
            players[players_of_round[j]]['games'] += 1

        player1 = players[players_of_round[0]]['name']
        player2 = players[players_of_round[1]]['name']
        player3 = players[players_of_round[2]]['name']
        player4 = players[players_of_round[3]]['name']

        schedule.append((player1, player2, player3, player4))

    print(tabulate.tabulate(schedule,
                            headers=['Player 1', 'Player 2', 'Player 3', 'Player 4'],
                            showindex="always",
                            tablefmt=tablefmt))
    print(tabulate.tabulate(players, headers={'Player': 'Games'}, showindex="never", tablefmt=tablefmt))


def pick_doubles(doubles: dict, double_candidates: list) -> list:
    """Pick the doubles from the list of candidates."""
    double_candidates.sort(key=lambda d: doubles[d])
    team1 = double_candidates[0]
    team2 = [(a, b) for a, b in double_candidates
             if a not in [team1[0], team1[1]] and b not in [team1[0], team1[1]]][0]
    return [team1, team2]


def permutate_doubles(players: list) -> list:
    """Create all possible doubles by the given player indices."""
    doubles = []

    for player in players:
        partners = [partner for partner in players if partner > player]
        for partner in partners:
            doubles.append((player, partner))

    return doubles



if __name__ == '__main__':
    main(prog_name='tennisschedule')
