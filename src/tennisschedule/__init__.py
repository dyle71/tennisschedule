#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ------------------------------------------------------------
# tennisschedule/__init__.py
#
# tennisschedule package.
#
# (C) Copyright 2023, see the 'LICENSE' file in the project root.
# Oliver Maurhart, headcode.space, https://headcode.space
# ------------------------------------------------------------

"""This is the tennisschedule package."""

