#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ------------------------------------------------------------
# tennisschedule/__main__.py
#
# tennisschedule package startup.
#
# (C) Copyright 2023, see the 'LICENSE' file in the project root.
# Oliver Maurhart, headcode.space, https://headcode.space
# ------------------------------------------------------------

"""This is the tennisschedule package start script."""

import tennisschedule.tennisschedule

if __name__ == '__main__':
    tennisschedule.tennisschedule.main(prog_name='tennisschedule')
