#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ------------------------------------------------------------
# tennisschedule/__about__.py
#
# tennisschedule package credits.
#
# (C) Copyright 2023, see the 'LICENSE' file in the project root.
# Oliver Maurhart, headcode.space, https://headcode.space
# ------------------------------------------------------------

"""This is the tennisschedule about."""

__author__ = 'Oliver Maurhart'
__email__ = '<oliver.maurhart@headcode.space>'
__copyright__ = '(C) Copyright 2023, Oliver Maurhart, headcode.space'
__license__ = 'MIT'
__title__ = 'tennisschedule'
__summary__ = """Tennis Schedule - generate pairings for a tennis tournament."""
__version__ = '0.1.0'
__uri__ = 'https://gitlab.com/dyle71/tennisschedule'
