# Tennis Schedule

Create the rounds for a tennis tournament. Inspired by a request of a friend of mine Armin Lipitz.

## Setup

I strongly recommend to work within a [virtual environment](https://docs.python.org/3/library/venv.html).

Prepare a virtual environment like this (you may have to install `python3-venv` beforehand):

```bash
mkdir venv
python3 -m venv venv
```

Then activate the virtual environment in a shell.

```bash
source  venv/bin/activate
```

As an alternative you may switch into the development environment by simply sourcing then
`env.sh` file:

```bash
source ./env.sh
```

This will pull in the virtual environment and will adjust `PYTHONPATH` and `PATH` variables
accordingly.


Next install the required packages:

```bash
pip3 install -r requirements.txt 
```

## Building

You can create source distributions (sdist) and wheels with this command:

```bash
python -m build
```

A folder `dist` will be created holding the package files.  

On a remote machine, you'll need a decent python3 (>= 3.8 will do) installment and pip installed.
Then copy the package of the `dist` folder to the machine and run:

```bash
sudo apt-get install python3 python3-pip
...
pip install tennisschedule-*.whl
```

## Running

You need a JSON file of players. An example can be found [here](./test/players.json).

Then you run the program with this:

```bash
tennisschedule --players players.json --rounds 20
```

## Notable guidelines

* How (not) to write git commit messages: https://www.codelord.net/2015/03/16/bad-commit-messages-hall-of-shame/
* How to version your software: https://semver.org/
* How to write a clever "Changes" file: https://keepachangelog.com/en/1.0.0/
* Folder Convention: https://github.com/KriaSoft/Folder-Structure-Conventions

---

(C) Copyright 2023  
Oliver Maurhart,   
headcode.space, https://headcode.space  

