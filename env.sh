# ------------------------------------------------------------
# env.sh
#
# Setup development environment.
#
#   Do not call this directly but source it into your current shell.
#
#           $ source ./env.sh
#
# (C) Copyright 2023, see the 'LICENSE' file in the project root.
# Oliver Maurhart, headcode.space, https://headcode.space
# ------------------------------------------------------------

echo "Setting up development environment..."

PROJECT_ROOT=$(dirname $(readlink -f $0))

# Check for python3 installment.
PYTHON3=$(which python3)
if [[ "$?" != "0" ]]; then
    echo "Did not found python3. Please install a version."
    exit 1
fi

# Setup virtual environment.
if [[ ! -d venv ]]; then
    ${PYTHON3} -m venv venv
fi

source venv/bin/activate
export PYTHONPATH=${PROJECT_ROOT}/src:${PROJECT_ROOT}/test
export PATH=${PROJECT_ROOT}/src/bin:${PATH}
export PROJECT_ROOT

