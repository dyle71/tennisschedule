#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ------------------------------------------------------------
# setup.py
#
# tennisschedule setuptools main file
#
# (C) Copyright 2023, see the 'LICENSE' file in the project root.
# Oliver Maurhart, headcode.space, https://headcode.space
# ------------------------------------------------------------

from setuptools import setup


def requirements():
    with open('requirements.txt', 'rt') as f:
        return [line.strip() for line in f.readlines()]


setup(
    name='tennisschedule',
    version='1.0.0',
    description='Create a roaster for tennis tournament.',
    long_description='Create a roaster for tennis tournament.',
    author='Oliver Maurhart',
    author_email='oliver.maurhart@headcode.space',
    maintainer='Oliver Maurhart',
    maintainer_email='oliver.maurhart@headcode.space',
    url='https://gitlab.com/dyle71/tennisschedule',
    license='MIT',

    # sources
    packages=['tennisschedule'],
    package_dir={'tennisschedule': 'src/tennisschedule'},
    scripts=['src/bin/tennisschedule'],

    # data
    include_package_data=True,
    data_files=[
        ('share/tennisschedule', ['requirements.txt'])
    ],

    install_requires=requirements()
)
