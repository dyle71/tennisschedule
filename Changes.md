# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]



[Unreleased]: https://www.gitlab.com/dyle71/keyquack/-/tree/develop
[1.0.0]: https://www.gitlab.com/dyle71/keyquack/-/releases/v1.0.0

---

Copyright (C) 2023 headcode.space e.U.  
Oliver Maurhart <info@headcode.space>  
[https://headcode.space](https://www.headcode.space)  
